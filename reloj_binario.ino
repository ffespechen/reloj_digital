// Adafruit NeoPixel library

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#include "RTClib.h"


#define PIN_M 6 // Pin del Neopixel que mostrará los Minutos
#define PIN_H 8 // Pin del Neopixel que mostrará las Horas


#define NUMPIXELS 8 // Cantidad de Pines de la tira

//Instancio las tiras de Neopixel
Adafruit_NeoPixel pixelsH(NUMPIXELS, PIN_H, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixelsM(NUMPIXELS, PIN_M, NEO_GRB + NEO_KHZ800);

// Instancio el Reloj de Tiempo Real
RTC_DS3231 rtc;

#define DELAYVAL 1000 // Tiempo de pausa


//Arreglos para almacenar la hora y minutos
int hora[] = {0,0,0,0,0,0,0,0};
int minutos[] = {0,0,0,0,0,0,0,0};

void setup() {
  // These lines are specifically to support the Adafruit Trinket 5V 16 MHz.
  // Any other board, you can remove this part (but no harm leaving it):
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  // END of Trinket-specific code.

  Serial.begin(9600);

  //Inicializo las tiras
  pixelsH.begin(); 
  pixelsM.begin();

  //Inicializo el reloj
  if (! rtc.begin()) {
    Serial.println("No se puede encontrar RTC");
    while (1);
  }

  if (rtc.lostPower()) {
    Serial.println("Actualizando la fecha y hora del reloj");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));

  }
}

void loop() {

  // Apago los LEDS
  pixelsH.clear(); 
  pixelsM.clear();

  DateTime dt = rtc.now();

  int hora_num = dt.hour();
  int min_num = dt.minute();

  Serial.print(hora_num);
  Serial.print(" : ");
  Serial.println(min_num);

  int indice = NUMPIXELS -1;

  while( hora_num >= 2 ){
    hora[indice] = hora_num % 2;
    indice--;

    if(hora_num/2==1)
    {
      hora[indice]=1;
    }

    hora_num = hora_num/2;
    
  }


  indice = NUMPIXELS -1;

  while( min_num >= 2 ){
    minutos[indice] = min_num % 2;
    indice--;

    if(min_num/2==1)
    {
      minutos[indice]=1;
    }

    min_num = min_num/2;
    
  }

  for(int i=0; i<NUMPIXELS; i++){
    if( hora[i]==1 ){
      pixelsH.setPixelColor(i, pixelsH.Color(0, 150, 0));
    }else{
      pixelsH.setPixelColor(i, pixelsH.Color(0, 0, 0));
    }
  }

  for(int i=0; i<NUMPIXELS; i++){
    if( minutos[i]==1 ){
      pixelsM.setPixelColor(i, pixelsM.Color(150, 0, 0));
    }else{
      pixelsM.setPixelColor(i, pixelsM.Color(0, 0, 0));
    }
  }    
      

  //Envío la info a las tiras
  pixelsH.show();   
  pixelsM.show();

  delay(DELAYVAL); 

}
